# The Horde

AI powered music groovebox.

## Current Features

* 11 instrument MIDI synthesizers
* 1 drum MIDI synthesizers
* 2 simulated Roland bass synthesizers (TB-303)
* 2 simulated Roland drum synthesizers (TR-808 or TR-909) 
* records entire session into WAV file
* partial UI interface

## Roadmap

* UI work
* more expressive sequencers
* live waveform display
* automation
* record/playback midi 
* physical midi input/input
* AI power

## Getting Started
prerequisites:
Java 1.8(only!) and Gradle

```
git clone https://github.com/raver1975/horde.git
gradle run
```
